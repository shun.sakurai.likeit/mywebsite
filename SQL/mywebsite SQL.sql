﻿CREATE DATABASE mywebsite CHARACTER SET utf8;
USE mywebsite;

/*userテーブル*/
CREATE TABLE t_user(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    login_id varchar(10) UNIQUE,
    login_password varchar(10),
    name varchar (10),
    birth_date date,
    gender varchar(5),
    postal_code int(11),
    address varchar(256),
    phone_number varchar(20),
    create_date date,
    update_date date
);

INSERT INTO t_user (login_id,login_password,name,birth_date,gender,postal_code,address,phone_number,create_date,update_date) VALUES
('saku_228','shunn228','管理者','1998-02-28','男性',3430015,'埼玉県越谷市花田５ー１８ー３４','080-5525-9875',now(),now());



/*buyテーブル*/                
CREATE TABLE t_buy(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    total_price int(11) NOT NULL,
    buy_date date NOT NULL,
    user_id int(11) NOT NULL,
    delivery_method_id int(11) NOT NULL,
);



/*delivery_methodテーブル*/                
CREATE TABLE m_delivery_method(
    id int(11) PRIMARY KEY AUTO_INCREMENT,  
    name varchar(256) NOT NULL,
    price int(11) NOT NULL,
);

INSERT INTO m_delivery_method (name, price) VALUES
(1, '特急配送', 500),
(2, '日時指定配送', 200),
(3, '普通配送', 0);



/*buy_detailテーブル*/
CREATE TABLE t_buy_detail(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    buy_id int(11) NOT NULL,
    item_id int(11) NOT NULL,
);



/*itemテーブル*/
CREATE TABLE m_item(
    id int(11) PRIMARY KEY AUTO_INCREMENT
    name varchar(256),
    category varchar(256),
    gender varchar(256),
    color varchar(256),
    price int(11),
    detail varchar(256),
    file_name varchar(256),
    create_date date,
    update_date date,
);

INSERT INTO m_item () VALUES
('コットンストレッチ/シェフイージーパンツ','パンツ','男性','白',4300,'シェフ達が仕事着として愛用されていたワークパンツしっかりとした丈夫な生地が特徴で、季節を問わずオールシーズン穿ける優秀な1本に仕上がっています。ウエストはゴムになっているので楽な履き心地が魅力。お出かけはもちろん、リラックスタイムにもおすすめです◎裾にかけて程良くテーパードをかけているので、すっきりとしたシルエットで着用いただけます。','https://www.dot-st.com/images/rageblue/goods/itemImg92/928660/item_928660_main_03_b.jpg',now(),now())


/*item_detailテーブル*/
CREATE TABLE m_item_detail(
    id int(11) PRIMARY KEY AUTO_INCREMENT
    item_id int(11)
    category_id int(11)
    gender_id int(11)
);

/*categoryテーブル*/
CREATE TABLE m_category(
    id int(11) PRIMARY KEY AUTO_INCREMENT
    category varchar(256)
);

INSERT INTO m_category VALUES
('パンツ'),('トップス'),('アウター'),('スカート'),('ワンピース・ドレス')；


/*genderテーブル*/
CREATE TABLE m_gender(
    id int(11) PRIMARY KEY AUTO_INCREMENT
    gender varchar(256)
);

INSERT INTO m_gender VALUES
('男性'),('女性');

DROP TABLE t_user;
